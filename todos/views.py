from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todolist(request):
    full_lst = TodoList.objects.all()
    context = {
        "todo_list_list": full_lst,
    }
    return render(request, "todos/list.html", context)


def show_todolist(request, id):
    specific_lst = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": specific_lst,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        # we use the form to validate the values
        # and save them to the DB
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        # create instance of django model form class
        form = TodoListForm()
    # put the form in the context
    context = {
        "form": form,
    }

    # render the HTML template with the form
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    specific_lst = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=specific_lst)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        # GET the recipeform for the requested recipe
        form = TodoListForm(instance=specific_lst)
    #
    context = {
        "form": form,
        "todo_list_detail": specific_lst,
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    specific_lst = TodoList.objects.get(id=id)
    if request.method == "POST":
        specific_lst.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


# create todo item
def todo_item_create(request):
    if request.method == "POST":
        # we use the form to validate the values
        # and save them to the DB
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        # create instance of django model form class
        form = TodoItemForm()
    # put the form in the context
    context = {
        "form": form,
    }

    # render the HTML template with the form
    return render(request, "todos/create-item.html", context)


def todo_item_update(request, id):
    specific_lst = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=specific_lst)
        if form.is_valid():
            # redirect to the current
            # detail page if form save success
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        # GET the recipeform for the requested recipe
        form = TodoItemForm(instance=specific_lst)
    #
    context = {
        "form": form,
        "todo_item_detail": specific_lst,
    }
    return render(request, "todos/update-item.html", context)
